/**
 * 
 */
package com.carlkuang.getalife;

import java.io.File;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

import com.carlkuang.getalife.helper.UtilLogger;
import com.carlkuang.getalife.helper.UtilLogger.LogLevel;

/**
 * @author WiseClock
 * @version 0.1
 */
public class GetALife extends BasicGame
{
    Image imgGroundTop;
    Image imgGroundBottom;
    Image imgLogo;
    int count = 0;

    public GetALife(String title)
    {
        super(title);
    }

    public void init(GameContainer container) throws SlickException
    {
        ModLoader.load();
        FontFactory.initialize();
        String pathGroundTop = new File(this.getClass().getResource("/com/carlkuang/getalife/resource/groundTop.png").getFile()).getAbsolutePath();
        String pathGroundHBottom = new File(this.getClass().getResource("/com/carlkuang/getalife/resource/groundBottom.png").getFile()).getAbsolutePath();
        String pathLogo = new File(this.getClass().getResource("/com/carlkuang/getalife/resource/menuLogo.png").getFile()).getAbsolutePath();
        imgGroundTop = new Image(pathGroundTop);
        imgGroundBottom = new Image(pathGroundHBottom);
        imgLogo = new Image(pathLogo);
        UtilLogger.log(LogLevel.INFO, "Game initialized.");
    }

    public void update(GameContainer container, int delta) throws SlickException
    {
        if(container.getInput().isKeyDown(Input.KEY_ENTER)){
            AppGameContainer gc = (AppGameContainer) container;
            switch (count)
            {
                case 0:
                    gc.setDisplayMode(1920, 1080, false);
                    count++;
                    break;
                case 1:
                    gc.setDisplayMode(1600, 900, false);
                    count++;
                    break;
                case 2:
                    gc.setDisplayMode(1280, 720, false);
                    count++;
                    break;
                case 3:
                    gc.setDisplayMode(800, 600, false);
                    count++;
                    break;
                case 4:
                    gc.setDisplayMode(640, 480, false);
                    count = 0;
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void mousePressed(int button, int x, int y)
    {
        System.out.println("click " + x + " " + y);
    }

    public void render(GameContainer container, Graphics graphics) throws SlickException
    {
        float currentHeight = container.getHeight();
        float scale = (float)(Math.round(currentHeight / 1080 * 100)) / 100;
        graphics.scale(scale, scale);

        graphics.setAntiAlias(false);

        graphics.setBackground(Color.magenta);

        //imgLogo.setFilter(Image.FILTER_NEAREST);
        imgLogo.drawCentered(960, 200);

        FontFactory.YAHEI.drawString(850, 242, "当个现充", Color.white);

        graphics.drawImage(imgGroundTop, 0, 952, 1920, 1016, 0, 0, 1920, 64);
        graphics.drawImage(imgGroundBottom, 0, 1016, 1920, 1080, 0, 0, 1920, 64);
    }

    public static void main(String[] argv) throws SlickException
    {
        AppGameContainer game = new AppGameContainer(new GetALife("当个现充"));
        game.setDisplayMode(1280, 720, false);
        game.setMaximumLogicUpdateInterval(60);
        //game.setTargetFrameRate(60);
        game.setAlwaysRender(true);
        //game.setVSync(true);
        game.setShowFPS(true);
        game.start();
    }
}
