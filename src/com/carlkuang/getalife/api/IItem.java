/**
 * 
 */
package com.carlkuang.getalife.api;

/**
 * @author WiseClock
 * @version 0.1
 */
public interface IItem
{
    String getItemId();
    String getItemDefaultName();
}
