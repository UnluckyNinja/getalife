/**
 * 
 */
package com.carlkuang.getalife.mod;

import com.carlkuang.getalife.GameRegistry;
import com.carlkuang.getalife.api.IItem;

/**
 * @author WiseClock
 * @version 0.1
 */
public class BasicMod
{
    protected final void registerItem(IItem item)
    {
        GameRegistry.registerItem(this, item);
    }
}
